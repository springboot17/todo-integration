package com.apress.todo.todointegration.service;

import com.apress.todo.todointegration.model.ToDo;
import lombok.RequiredArgsConstructor;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SendMessage {

    private final MessageChannel input;

    @Scheduled(fixedRate = 5000L)
    public void sendToInput() {
        input.send(MessageBuilder.withPayload(new ToDo("Отправлено в инпут", true)).build());
    }

}
