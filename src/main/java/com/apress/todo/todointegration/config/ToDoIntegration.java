package com.apress.todo.todointegration.config;

import java.util.Locale;

import com.apress.todo.todointegration.model.ToDo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.MessageChannels;

@EnableIntegration
@Configuration
public class ToDoIntegration {

    /**
     * Регистрируем канал.
     */
    @Bean
    public DirectChannel input() {
        return MessageChannels.direct().get();
    }

    /**
     * Обработака сообщений , который приходят в канал DirectChannel и отправляются в канал -> channelNow.
     */
    @Bean
    public IntegrationFlow simpleFlow() {
        return IntegrationFlows
                .from(input())
                .filter(ToDo.class, ToDo::isCompleted)
                .transform(ToDo.class, toDo -> toDo.getDescription().toUpperCase(Locale.ROOT))
                .channel("channelNow")
                .handle(System.out::println)
                .get();
    }

    /**
     * Обработака сообщений , который приходят в канал channelNow.
     */
    @Bean
    public IntegrationFlow simpleFlowChannelNow() {
        return IntegrationFlows
                .from("channelNow")
                .transform(String.class, toDo -> toDo + " новый канал принял")
                .handle(System.out::println)
                .get();
    }

}
